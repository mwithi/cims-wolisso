# -*- encoding: utf-8 -*-

{
    'name': 'Wolisso - Asset Data',
    'version': '0.9',
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'description': """
St.Luke Hospital of Wolisso Asset Data (first import)
=====================================================

The module imports:

* 1 journal ASSJ
* 8 new account accounts
* 15 new analytic account
* 475 asset locations
* 22 asset categories
* 6094 assets

It requires following modules installed:

* account_asset_management
* account_asset_register
* isf_asset_extesion

    """,
    'license': 'AGPL-3',
    'category': 'Localization',
    'data': ['data/account.journal.csv',
             'data/account.account.csv',
             'data/account.analytic.account.csv',
             'data/isf.asset.location.csv',  
             'data/account.asset.category.csv',
             'data/account.asset.asset.csv',
             ],
    'demo': [],
    'installable': True,
    'auto_install': False
}

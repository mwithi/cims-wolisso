# -*- coding: utf-8 -*-

{
	'name': 'ISF St.Luke Monthly Key Data Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF St.Luke Monthly Key Data Report 
===================================

St.Luke Monthly Key Data Report schema
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	#'depends': ['isf_cims_module','isf_treasury_budget_report'],
	'data': [
		'data/isf.monthly.key.data.report.csv',
        'isf_wolisso_monthly_key_data_report.xml',
    ],
	'demo': [],
	'installable' : True,
}


# -*- coding: utf-8 -*-

{
	'name': 'ISF St.Luke Treasury Budget Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF St.Luke Treasury Budget Report 
======================================

St.Luke Treasury Budget Report schema
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	#'depends': ['isf_cims_module','isf_treasury_budget_report'],
	'data': [
		'data/isf.treasury.report.csv',
        'isf_wolisso_treasury_report.xml',
    ],
	'demo': [],
	'installable' : True,
}


# -*- encoding: utf-8 -*-

{
    'name': 'St.Luke Hospital of Wolisso - Accounting',
    'version': '0.8',
    'depends': ['account_accountant','account_check_writing'],
    'author': 'Alessandro Domanico for ISF',
    'description': """
St.Luke Hospital of Wolisso Chart of Accounts
================================================

St.Luke Hospital of Wolisso accounting chart and localization.
    """,
    'license': 'AGPL-3',
    'category': 'Localization/Account Charts',
    'data': [
		#'security/ir.model.access.csv', <-- file present just as example
        #'data/account.account.type.csv',
        #'data/account.account.template.csv',
		#'data/account.tax.code.template.csv',
		'account_chart.xml',
		'base_data.xml',
		'currency_data.xml',
		'data/account.tax.template.csv',
		'data/account.fiscal.position.template.csv',
        'l10n_chart_en_wolisso_generic.xml',
        ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
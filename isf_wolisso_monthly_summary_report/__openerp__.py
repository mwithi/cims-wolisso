# -*- coding: utf-8 -*-

{
	'name': 'ISF St.Luke Monthly Summary Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF St.Luke Monthly Summary Report 
===================================

St.Luke Monthly Summary Report schema
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	#'depends': ['isf_cims_module','isf_treasury_budget_report'],
	'data': [
		'data/isf.monthly.summary.report.csv',
        'isf_monthly_summary_report.xml',
    ],
	'demo': [],
	'installable' : True,
}

